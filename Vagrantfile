# -*- mode: ruby -*-
# vi: set ft=ruby :

SUBNET = "192.168.60."
MEM_NODE = 512
MEM_CLIENT = 512
MEM_SERVER = 4096
DEF_HOSTS = "hosts"
BOX_CLIENT = "bento/ubuntu-14.04"

machines = [
  {
    :name => "server.dev",
    :ip => "12",
    :box => "bento/ubuntu-16.04",
    :playbook => "server.yml",
    :inventory => DEF_HOSTS,
    :memory => MEM_SERVER
  },
  {
    :name => "node.dev",
    :ip => "13",
    :box => "centos/7",
    :playbook => "node.yml",
    :inventory => DEF_HOSTS,
    :guest_port => 80,
    :host_port => 8080,
    :memory => MEM_NODE
  },
  {
    :name => "client.dev",
    :ip => "11",
    :box => BOX_CLIENT,
    :playbook => "client.yml",
    :inventory => DEF_HOSTS,
    :memory => MEM_CLIENT
  },
  {
    :name => "client2.dev",
    :ip => "14",
    :box => BOX_CLIENT,
    :playbook => "client2.yml",
    :inventory => DEF_HOSTS,
    :memory => MEM_CLIENT
  },
  {
    :name => "workstation.dev",
    :ip => "15",
    :box => "bento/ubuntu-18.04",
    :inventory => DEF_HOSTS,
    :playbook => "workstation.yml",
    :memory => MEM_CLIENT
  },
  {
    :name => "habitat.dev",
    :ip => "16",
    :box => "bento/ubuntu-18.04",
    :inventory => DEF_HOSTS,
    :playbook => "habitat.yml",
    :memory => MEM_CLIENT,
    :vault_pass => "vault_pass.yml"
  }
]

Vagrant.configure("2") do |config|
  machines.each do |opts|
    config.vm.define opts[:name] do |node|
      node.vm.box = opts[:box]
      node.ssh.insert_key = false
      node.vm.provider :virtualbox do |v|
        v.memory = opts[:memory]
        v.linked_clone = true
      end
      node.vm.hostname = opts[:name]
      node.vm.network :private_network, ip: SUBNET + opts[:ip]
      if (opts.has_key?(:host_port) and opts.has_key?(:guest_port)) then
        node.vm.network :forwarded_port, guest: opts[:guest_port], host: opts[:host_port], host_ip: "127.0.0.1"
      end
      node.vm.provision :ansible do |p|
        p.playbook = opts[:playbook]
        p.inventory_path = opts[:inventory]
        p.galaxy_role_file = "requirements.yml"
        p.extra_vars = {
          ansible_ssh_user: 'vagrant',
          ansible_ssh_private_key_file: "~/.vagrant.d/insecure_private_key",
          ansible_host: SUBNET + opts[:ip]
        }
        if opts.has_key?(:vault_pass) then
          p.vault_password_file = opts[:vault_pass]
        end
      end
    end
  end
end
