apt_update 'update cache' do
  frequency 86_400
  action :periodic
end

package 'httpd'

service 'httpd' do
  action [:enable, :start]
end

file '/var/www/html/index.html' do
  content '<html>
  <body>
    <h1>a heading</h1>
  </body>
</html>'
end
